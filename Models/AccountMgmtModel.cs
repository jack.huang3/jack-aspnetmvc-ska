﻿namespace MVC_Traning.Models;



public class AccountMgmtProps   
{
  public string? Name { get; set; }
  public double? Amount { get; set; }
  public DateTime? Date { get; set; }
  public string? Remark { get; set; }
}

public class AccountMgmtModel
{
    public List<AccountMgmtProps> AccountListData { get; set; } = default!;
}
