﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MVC_Traning.Models;
using MVC_Traning.Services;

namespace MVC_Traning.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly AccountMgmtService _accountMgmtService;

    private static readonly List<AccountMgmtProps> AccountListData = new List<AccountMgmtProps>();

    public HomeController(ILogger<HomeController> logger, AccountMgmtService accountMgmtService)
    {
        _logger = logger;
        _accountMgmtService = accountMgmtService;
    }

    public IActionResult Index()
    {
        return View(GetAccountData());
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    private AccountMgmtModel GetAccountData()
    {
        AccountMgmtProps accountData = new AccountMgmtProps()
        {
            Amount = 100,
            Name = "Jack",
            Date = new DateTime(2022, 12, 8),
            Remark = "Test"
        };
        AccountListData.Add(accountData);
        return new AccountMgmtModel() { AccountListData = AccountListData };
    }
}